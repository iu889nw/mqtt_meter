package com.example.metering;

import com.example.metering.mqtt.MqttPublishSubscribeUtility;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class MqttPublishSubscribeUtilityTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MqttPublishSubscribeUtilityTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MqttPublishSubscribeUtilityTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	MqttPublishSubscribeUtility mqttPublishSubscribeUtility = new MqttPublishSubscribeUtility();
    }
}
