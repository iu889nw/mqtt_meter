package com.example.metering.mqtt;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.google.gson.JsonObject;

/**
 * This is the MQTT Callback class which overrides the MQTT Call back methods 
 *
 */
class SimpleCallback implements MqttCallback {

	final CountDownLatch latch = new CountDownLatch(1);

	//Called when the client lost the connection to the broker
	public void connectionLost(Throwable arg0) {
		System.out.println("Connection lost to the broker tcp://192.168.100.161:1883");
		latch.countDown();
	}
	
    //Called when a new message has arrived
    public void messageArrived(String topic, MqttMessage message) throws Exception {
		String time = new Timestamp(System.currentTimeMillis()).toString();
        System.out.println("-------------------------------------------------");
		System.out.println("| Time:" + time);
		System.out.println("| Topic:" + topic);
        System.out.println("| Message: " + new String(message.getPayload()));
        System.out.println("-------------------------------------------------");
		latch.countDown(); // unblock main thread
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
		System.out.println("Delivery is Complete");
		
	}
}

/**
 * This class holds the MQTT methods to Connect, Publish & Subscribe to Broker
 *
 */
public class MqttPublishSubscribeUtility {
	
	private final static String PROPERTIES_FILE_NAME = "/mqtt.properties";

	Properties props = new Properties();
	String broker = "tcp://192.168.100.161:1883";
	String clientId = "subscriber";

	public MqttClient mqttConnect() throws MqttException{

		MemoryPersistence persistence = new MemoryPersistence();
		/**
		  * Load device properties
		  */
		try {
			props.load(MqttPublishSubscribeUtility.class.getResourceAsStream(PROPERTIES_FILE_NAME));

		} catch (IOException e) {
			System.err.println("Not able to read the properties file, exiting..");
			System.exit(-1);
		}
		System.out.println("About to connect to MQTT broker with the following parameters: - BROKER_URL=" + props.getProperty("BROKER_URL")+" CLIENT_ID="+props.getProperty("CLIENT_ID"));
		MqttClient sampleClient = new MqttClient(broker, clientId,persistence);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
//		connOpts.setPassword(props.getProperty("PASSWORD").toCharArray());
//		connOpts.setUserName(props.getProperty("USERNAME"));
        sampleClient.connect(connOpts);
//        System.out.println("Connected");
		return sampleClient;
	}
	
	public void mqttConnectNPublishNSubscribe(){
	    try {
	    	MqttClient sampleClient = mqttConnect();
	        sampleClient.subscribe(props.getProperty("TOPIC_NAME"), 1);
	        sampleClient.setCallback(new SimpleCallback());
	    } catch(MqttException me){
	        System.out.println("reason " + me.getReasonCode());
	        System.out.println("msg " + me.getMessage());
	        System.out.println("loc " + me.getLocalizedMessage());
	        System.out.println("cause " + me.getCause());
	        System.out.println("except " + me);
	        me.printStackTrace();
	    }
	}
}