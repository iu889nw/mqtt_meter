package com.example.metering.repos;

import com.example.metering.model.PowerModel;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by FS0413 on 8.8.2017.
 */
public interface PowerRepository extends CrudRepository<PowerModel,Long> {
}
