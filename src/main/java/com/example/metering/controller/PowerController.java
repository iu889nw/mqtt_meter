package com.example.metering.controller;

import com.example.metering.model.PowerModel;
import com.example.metering.mqtt.MqttPublishSubscribeUtility;
import com.example.metering.mqtt.TopicSubscriber;
import com.example.metering.repos.PowerRepository;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by FS0413 on 8.8.2017.
 */
@Controller
@RequestMapping("")
public class PowerController {

    @Autowired
    private PowerRepository powerRepo;

    @RequestMapping(value="/index",method = RequestMethod.GET)
    public String dashboard(Model model) throws Exception {

        PowerModel power = new PowerModel();
        MqttPublishSubscribeUtility mqttPublishSubscribeUtility = new MqttPublishSubscribeUtility();
        JsonObject payload = new JsonObject();
        TopicSubscriber subscriber = new TopicSubscriber();
        subscriber.run("localhost:1883");
        return "index";
    }

}
