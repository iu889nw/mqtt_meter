package com.example.metering.model;


import javax.persistence.*;

/**
 * Created by FS0413 on 8.8.2017.
 */
@Entity
@Table(name = "consumption")
public class PowerModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private long id;

    @Column(name="timestamp")
    private Integer timestamp;

    @Column(name ="power")
    private Double power;

    public PowerModel() {
    }

    public PowerModel(Integer timestamp, Double power) {
        this.timestamp = timestamp;
        this.power = power;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public Double getPower() {
        return power;
    }

    public void setPower(Double power) {
        this.power = power;
    }
}
